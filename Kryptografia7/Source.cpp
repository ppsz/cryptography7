#include <iostream>
#include <random>
#include <array>

unsigned long long g_private_key;
std::vector<int> g_fermat_result;

long long int ExpMod(unsigned long long base, unsigned long long exponent, unsigned long long modulo, unsigned long long message = 1)
{
	unsigned long long temp = 1, result = 0;

	do
	{
		if (exponent % 2 == 1)
			temp = (temp * base) % modulo;
		base = (base * base) % modulo;
		exponent /= 2;
	} while (exponent > 0);
	result = (message*temp) % modulo;
	return result;
}

unsigned long long GeneratePublicKey(unsigned long long message, unsigned long long p, unsigned long long r)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<unsigned long long> dist(2, p - 2);
	unsigned long long a;
	g_private_key = dist(mt);
	//g_private_key = 6739;
	a = ExpMod(r, g_private_key, p);

	return a;
}

std::array<unsigned long long, 2> Encrypt(unsigned long long message, unsigned long long p, unsigned long long r, unsigned long long a)
{
	unsigned long long C1, C2, j;
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<unsigned long long> dist(2, p - 2);
	j = dist(mt);
	//j = 34310;

	C1 = ExpMod(r, j, p);
	C2 = ExpMod(a, j, p, message);

	std::cout << "C1: " << C1 << " C2: " << C2 << "\n";
	return std::array<unsigned long long, 2>{C1,C2};
}

void Decrypt(unsigned long long p, unsigned long long C1, unsigned long long C2)
{
	unsigned long long t, P;

	t = ExpMod(C1, p - 1 - g_private_key, p);
	P = ExpMod(C2*t, 1, p);

	std::cout << "Decrypted: " << P << "\n";
}

void Fermat(unsigned int number)
{
	unsigned long long x, y, y_2, factor_1, factor_2;

	while (number % 2 == 0)
	{
		number /= 2;
		//std::cout << 2 << " ";
		g_fermat_result.push_back(2);
	}
	if (number > 1)
	{
		x = static_cast<unsigned long long>(ceil(sqrt(number)));
		do
		{
			y_2 = x * x - number;
			y = static_cast<unsigned long long>(floor(sqrt(y_2)));
			if (y_2 == y * y)
			{
				factor_1 = x - y;
				factor_2 = x + y;
				if (factor_1 == 1)
					break;
				Fermat(factor_1);
				Fermat(factor_2);
				return;
			}
			x++;
		} while (x + y < number);
		//std::cout << number << " ";
		g_fermat_result.push_back(number);
	}
}

bool Lucas(long long n, long long b)
{
	bool prime = false;
	int last_element = 0;
	Fermat(n - 1);
	for (auto element : g_fermat_result)
	{
		if (element == last_element)
			continue;

		if (ExpMod(b, ((n - 1) / element), n) == 1)
		{
			return false;
		}
	}
	return true;
}
int main()
{
	unsigned long long message = 300;
	unsigned long long p = 37813;

	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<unsigned long long> dist(2, p - 2);
	unsigned long long r;// = 36410;
	do
	{
		r = dist(mt);
		Fermat(r);

	} while (!Lucas(p, r));
	
	std::cout << r << " -r\n";

	unsigned long long a;
	std::array<unsigned long long, 2> C;

	a = GeneratePublicKey(message, p, r);
	C = Encrypt(message, p, r, a);

	Decrypt(p, C[0], C[1]);

	system("pause");
}